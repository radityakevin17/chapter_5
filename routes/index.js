const router = require('express').Router()
const {getAllUsers, renderPageForChap3, renderPageForChapter4, renderjs } = require('../controler')


router.get('/users', getAllUsers)
router.get('/chap3', renderPageForChap3)
router.get('/chap4', renderPageForChapter4)





module.exports = router