const users = require('../models/users.json')



async function getAllUsers(req, res, next){
    try {
        res.json({
            status : 200,
            message : "berhasil mendapatkan data",
            data : users
        })
        
    } catch (error) {
        next(error)
        
    }
}

async function renderPageForChap3(req, res, next){
    try {
        res.render('chap3')
        
    } catch (error) {
        next(error)
        
    }
}


async function renderPageForChapter4(req, res, next){
    try {
        res.render('chap4')
    } catch (error) {
        next(error)
        
    }
}





module.exports = {
    getAllUsers,
    renderPageForChap3,
    renderPageForChapter4,
}