const { urlencoded, json } = require('express')
const express = require('express')
const { route } = require('./routes')
const app = express()
const router = require('./routes')
const port = 4000

app.use('/static', express.static(__dirname + '/public'))
app.use(json())
app.use(urlencoded({extended : true}))
app.set('view engine', 'ejs')

app.use(router)

app.listen(port,() => {
    console.log('berhasil')
})